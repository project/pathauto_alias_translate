<?php

/**
 * @file
 * Pathauto Alias Translate module main file.
 */

/**
 * Implements hook_menu().
 */
function pathauto_alias_translate_menu() {
  $items = array();
  $items['admin/config/search/path/pathauto_alias_translate'] = array(
    'type' => MENU_LOCAL_TASK,
    'title' => 'Translation settings',
    'description' => 'Pathauto Alias Translate module settings',
    'page arguments' => array('pathauto_alias_translate_config_form'),
    'page callback' => 'drupal_get_form',
    'weight' => 25,
    'access arguments' => array('administer pathauto'),
  );

  return $items;
}

/**
 * Module's settings form.
 */
function pathauto_alias_translate_config_form($form, &$form_state) {
  $form['#prefix'] = '<div id="pathauto-alias-translate-settings">';
  $form['#suffix'] = '</div>';
  $form['pathauto_alias_translate_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Yandex Translate API key'),
    '#default_value' => variable_get('pathauto_alias_translate_key', ''),
    '#description' => t('<a href="http://translate.yandex.ru/" target="_blank">Yandex Translate</a> API key, which you can get <a href="https://tech.yandex.ru/keys/get/?service=trnsl" target="_blank">on the service page</a> for free, or <a href="https://tech.yandex.ru/keys/" target="_blank">in your list of keys</a>, if you already have one.'),
    '#ajax' => array(
      'event' => 'change',
      'callback' => 'pathauto_alias_translate_show_languages',
      'method' => 'replace',
      'wrapper' => 'pathauto-alias-translate-settings',
      'effect' => 'fade',
    ),
  );
  // Check API key for existence and validity and show available languages.
  if ((isset($form_state['values']['pathauto_alias_translate_key']) && $key = $form_state['values']['pathauto_alias_translate_key']) || $key = $form['pathauto_alias_translate_key']['#default_value']) {
    $site_language = language_default('language');
    $query_for_supported_languages = drupal_http_request("https://translate.yandex.net/api/v1.5/tr.json/getLangs?key=$key&ui=$site_language");
    if ($query_for_supported_languages->code == 200 && $query_for_supported_languages->status_message == 'OK') {
      $supported_languages = (array) json_decode($query_for_supported_languages->data)->langs;
      natsort($supported_languages);
      $supported_languages = array_merge(array('default' => t('Default site language')), $supported_languages);
      $form['pathauto_alias_translate_language_from'] = array(
        '#type' => 'select',
        '#title' => t('Source language'),
        '#options' => $supported_languages,
        '#default_value' => variable_get('pathauto_alias_translate_language_from', FALSE) ? variable_get('pathauto_alias_translate_language_from') : $site_language,
        '#description' => t('Choose source language (optional). It may improve translation accuracy. By default site-wide language will be chosen.'),
      );
    }
    else {
      drupal_set_message(json_decode($query_for_supported_languages->data)->message, 'error');
    }
  }
  $form['pathauto_alias_translate'] = array(
    '#type' => 'checkbox',
    '#title' => t('Translate prior to creating alias'),
    '#default_value' => variable_get('pathauto_alias_translate', FALSE),
    '#description' => t('Translates alias from local language into English. Notice: it will disable "Translate option".'),
    '#states' => array(
      'disabled' => array(
        '[name="pathauto_alias_translate_key"]' => array(
          'filled' => FALSE,
        ),
      ),
    ),
    '#access' => module_exists('pathauto'),
  );
  $form['#validate'][] = 'pathauto_alias_translate_extra_validate';
  return system_settings_form($form);
}

/**
 * Ajax callback for pathauto_alias_translate_config_form.
 */
function pathauto_alias_translate_show_languages($form, $form_state) {
  return system_settings_form($form);
}

/**
 * Implements hook_form_FORM_ID_alter().
 *
 * We add a checkbox to the Pathauto module settings form to be able to switch
 * alias translations from there.
 */
function pathauto_alias_translate_form_pathauto_settings_form_alter(&$form, &$form_state) {
  $form['pathauto_alias_translate'] = array(
    '#type' => 'checkbox',
    '#title' => t('Translate prior to creating alias'),
    '#default_value' => variable_get('pathauto_alias_translate', FALSE),
    '#description' => t('Translates alias from local language into English (needed API key provided by <a href="http://translate.yandex.ru/" target="_blank">Yandex Translate</a> service, which you can set <a href="@url">on this page</a>)', array('@url' => url('admin/config/search/pathauto_alias_translate'))),
    '#disabled' => !variable_get('pathauto_alias_translate_key', FALSE),
    '#access' => module_exists('pathauto'),
    '#ajax' => array(
      'event' => 'change',
      'callback' => '_pathauto_alias_translate_check_transliterate',
    ),
  );
  $form['pathauto_transliterate']['#ajax'] = array(
    'event' => 'change',
    'callback' => '_pathauto_alias_translate_check_translate',
  );
}

/**
 * Validate callback for pathauto_alias_translate_config_form.
 *
 * Check API key for validity and switch off translation if it's invalid.
 */
function pathauto_alias_translate_extra_validate(&$form, &$form_state) {
  $key = $form_state['values']['pathauto_alias_translate_key'];
  $query_for_supported_languages = drupal_http_request("https://translate.yandex.net/api/v1.5/tr.json/getLangs?key=$key&ui=en");
  if ($query_for_supported_languages->code !== '200') {
    drupal_set_message(t('According to errors an alias translating is switched off. <br>Error message text: @message', array('@message' => json_decode($query_for_supported_languages->data)->message)), 'warning');
    $form_state['values']['pathauto_alias_translate'] = 0;
  }
  elseif ($form_state['values']['pathauto_alias_translate']) {
    variable_set('pathauto_transliterate', 0);
  }
}

/**
 * Ajax callback function for pathauto_settings_form.
 *
 * Switch off pathauto transliterate function if translate checkbox is chosen.
 */
function _pathauto_alias_translate_check_transliterate(&$form, &$form_state) {
  if ($form_state['values']['pathauto_alias_translate']) {
    return array(
      '#type' => 'ajax',
      '#commands' => array(
        ajax_command_invoke('[name="pathauto_transliterate"]', 'val', array(array('checked', FALSE))),
      ),
    );
  }
}

/**
 * Ajax callback function for pathauto_settings_form.
 *
 * Switch off pathauto translate function if transliterate checkbox is chosen.
 */
function _pathauto_alias_translate_check_translate(&$form, &$form_state) {
  if ($form_state['values']['pathauto_transliterate']) {
    return array(
      '#type' => 'ajax',
      '#commands' => array(
        ajax_command_invoke('[name="pathauto_alias_translate"]', 'val', array(array('checked', FALSE))),
      ),
    );
  }
}

/**
 * Implements hook_pathauto_alias_alter().
 */
function pathauto_alias_translate_pathauto_alias_alter(&$alias, array &$context) {
  $alias = _pathauto_alias_translate_translate($alias);
}

/**
 * Transliteration callback.
 *
 * @param string $text
 *   The text to translate.
 */
function _pathauto_alias_translate_translate($text = '') {
  $key = variable_get('pathauto_alias_translate_key', '');
  $lang_from_set = variable_get('pathauto_alias_translate_language_from', FALSE);
  $lang_from = ($lang_from_set && $lang_from_set !== 'default') ? ($lang_from_set . '-') : (language_default('language') . '-');
  if (!$text || !$key) {
    return FALSE;
  }
  $text = urlencode($text);
  $url = "https://translate.yandex.net/api/v1.5/tr.json/translate?key=${key}&text=$text&lang=${lang_from}en&format=plain";
  $query = drupal_http_request($url);
  if (isset($query->data)) {
    $response = json_decode($query->data);
    if ($query->code == '200') {
      if (isset($response->text[0])) {
        return $response->text[0];
      }
    }
    else {
      drupal_set_message(t('Alias translation error: @message', array('@message' => $response->message)));
      if ($query->code == '404') {
        watchdog('pathauto_alias_translate', 'Translation limit has been eceeded.', array(), WATCHDOG_NOTICE, l(t('Request for paid version'), 'mailto:translate-business@support.yandex.com'));
        // Check for earlier sent mails to avoid multiple messaging.
        if (!variable_get('pathauto_alias_translate_has_limit', FALSE)) {
          // Notify administrator about exceeding translations limit.
          $extra_message = module_exists('transliteration') ? 'Alias processing has been fall back to transliteration.' : '';
          drupal_mail('pathauto_alias_translate', 'pathauto_alias_translate_limit_notification', variable_get('site_mail', ''), language_default(), array('message' => t('Translation limit has been exceeded. You may response for paid account, writing letter to translate-business@support.yandex.com. Current prices can be found on the service page: https://translate.yandex.ru/developers/prices. @extra_message', array('@extra_message' => $extra_message))));
          variable_set('pathauto_alias_translate_has_limit', TRUE);
        }
      }
    }
    if (module_exists('transliteration')) {
      return transliteration_get(urldecode($text));
    }
  }
  return FALSE;
}

/**
 * Implements hook_mail().
 */
function pathauto_alias_translate_mail($key, &$message, $params) {
  if ($key == 'pathauto_alias_translate_limit_notification') {
    $message['subject'] = t('Translation limit acceded');
    $message['body'][] = $params['message'];
  }
}

/**
 * Implements hook_cron().
 */
function pathauto_alias_translate_cron() {
  variable_set('pathauto_alias_translate_has_limit', FALSE);
}

/**
 * Implements hook_form_BASE_FORM_alter().
 */
function pathauto_alias_translate_form_node_form_alter(&$form, &$form_state) {
  if (variable_get('pathauto_alias_translate')) {
    $form['path']['#description'] = t('Translating is provided by <a href="http://translate.yandex.ru" target="_blank">Yandex.Translate</a> service.');
  }
}
