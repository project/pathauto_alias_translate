-- SUMMARY --

This module adds ability for Pathauto to translate aliases into English
using Yandex Translate API, which is still free by this moment.
This module may improve your SEO, making your local language URLs
not just transliterated but qualitatively translated into English.

-- REQUIREMENTS --

Pathauto.

-- INSTALLATION --

Install as usual, see http://drupal.org/node/895232 for further information

-- CONFIGURATION --

* The module requires "administer pathauto" permission provided by
the Pathauto module to change its setting.
* Get Yandex Translate API key on the service page
(https://tech.yandex.ru/keys/get/?service=trnsl).
* Add your Yandex Translate API key on the module settings page
(admin/config/search/path/pathauto_alias_translate).
* Check setting "Translate prior to creating alias" on the module settings page
or on the Pathauto settings page.

-- CONTACT --

Current maintainers:
* Aleksey Zubenko (Alex Zu) - https://www.drupal.org/user/1797134
